const contacts = [
    {
      id: 1,
      name: "Jonah",
      email: "jdelnero@pizzamail.dough",
    },
    {
      id: 2,
      name: "Elon",
      email: "elon@pizzamail.mars",
    },
    {
      id: 3,
      name: "Warren",
      email: "warren@pizza.buffet",
    }
  ];

  module.exports = contacts;