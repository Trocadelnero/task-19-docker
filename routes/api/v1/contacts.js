const express = require('express');
const router = express.Router();
const contacts = require("../../../Contacts");



const emailSentResponse = ({msg: `Thank you for sending an e-mail`});
//Contacts GET
router.get("/", (req, res) => {
    res.status(200).json(contacts);
  });
  
  //Get single contact
  router.get("/:id", (req, res) => {
    const found = contacts.some(contact => contact.id === parseInt(req.params.id));
  
    if(found) {
      res.status(200).json(contacts.filter(contacts => contacts.id === parseInt(req.params.id)));
    } else {
      res.status(400).json({ msg: `No member with the id of ${req.params.id}` });
    }
    });
  // Contacts - POST
  router.post("/", (req, res) => {
     const newContact = {
       name: req.params.name,
       email: req.params.email,
     };
     contacts.push(newContact);
    
     
    res.status(201).json(emailSentResponse);
  });


  module.exports = router;
  