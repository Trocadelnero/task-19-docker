#1.Define base image
FROM node:latest

#2.specify/create work dir
WORKDIR /usr/src/app

#3Copy dependencies from json + json.lock docker-node > usr/src/app
COPY package*.json ./

RUN npm install

#4. Bundle source code, copy to workdir
COPY . .

#5 Expose port
EXPOSE 3000:3000

#6
CMD ["node", "app.js"]